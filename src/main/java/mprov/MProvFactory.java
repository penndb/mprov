package mprov;

import java.util.HashMap;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.upenn.cis.db.habitat.core.api.ProvenanceApi;
import edu.upenn.cis.db.habitat.core.api.mocks.InMemoryProvAPI;
import edu.upenn.cis.db.habitat.provenance.ProvStorageModule;
import edu.upenn.cis.db.habitat.repository.storage.neo4j.NeoStorageModule;
import jep.Jep;
import jep.JepConfig;
import jep.JepException;


public class MProvFactory {
	
	static ProvenanceApi store = null;
	public static int executorId = 1;
	static HashMap<String,String> sensorMap = new HashMap<>();
	//private static Jep jep = null;
	/*public static Jep getJep() {
		return jep;
	}*/

	/**
	 * Get the provenance store.  Ultimately this will go to a REST microservice.
	 * 
	 * @return
	 */
	public static synchronized ProvenanceApi getProvenanceStore() {
		if (store == null){
			Injector injector = Guice.createInjector(
					new NeoStorageModule(),
					new ProvStorageModule()
					);
			
			store = injector.getInstance(ProvenanceApi.class);
		}
		
		return store;
	}
	
	public static String getWindowID(String operator, int id) {
		//return "w" + operator + "." + id;
		return "w." + id;
	}
	

	public static String getResultID(String operator, int id) {
		//return "r" + operator + "." + id;
		return "r." + id;
	}

	public static String getEntityID(String operator, int id) {
		//return "e" + operator + "." + id;
		return "e." + id;
	}
	
	public static String getActivityID(String operator, int id) {
		//return "a" + operator + "." + id;
		return "a." + id;
	}
	
	public static String getSensorName(String sensorId) {
		return sensorMap.get(sensorId);
	}
	
	public static void setSensorName(String sensorId, String sensorName) {
		sensorMap.put(sensorId, sensorName);
	}
	
	
	public static void initializeJep(){
		try {
			Jep jep = new Jep(new JepConfig().addSharedModules("numpy").addSharedModules("scipy"));
			jep.eval("from java.lang import System");
		    jep.eval("import numpy");
		    jep.eval("import scipy");
			jep.eval("import sys");
			jep.eval("import os");
			jep.eval("os.environ['SPARK_HOME']='/usr/local/spark'");
			jep.eval("os.environ['PYTHONPATH']=os.environ['SPARK_HOME']+'/python/lib/py4j-0.10.4-src.zip:'+os.environ['SPARK_HOME']+'/python:'+os.environ['PATH']");
			jep.eval("import findspark");
			jep.eval("findspark.init()");
			jep.eval("os.environ['PYSPARK_PYTHONPATH_SET']='1'");
			jep.eval("sys.path.append(\"/Users/rishabhgupta/GitHub/CerebralCortex\")");
			jep.eval("sys.path.append(\"/usr/local/spark/python\")");
			jep.eval("sys.path.append(\"/usr/local/spark/python/lib\")");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct");
			jep.eval("from cerebralcortex.kernel.datatypes.datastream import DataStream");
			jep.eval("from cerebralcortex.data_processor.preprocessor import parser");
			jep.eval("from datetime import datetime, timedelta");
			//jep.eval("from cerebralcortex.data_processor.cStress import cStress");
			jep.eval("from cerebralcortex.data_processor.feature.ecg import ecg_feature_computation");
			jep.eval("from cerebralcortex.data_processor.feature.feature_vector import generate_cStress_feature_vector");
			jep.eval("from cerebralcortex.data_processor.feature.rip import rip_cycle_feature_computation");
			jep.eval("from cerebralcortex.data_processor.feature.rip import window_rip");
			jep.eval("from cerebralcortex.data_processor.signalprocessing import rip");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.accelerometer import accelerometer_features");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct, autosense_sequence_align");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import compute_outlier_ecg");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import ecg_data_quality");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import rip_data_quality");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.ecg import compute_rr_intervals");
			jep.eval("from cerebralcortex.model_development.model_development import analyze_events_with_features");
			jep.close();
		} catch (JepException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Jep initialized");
	}
}
