package mprov;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseWindowedBolt.Count;
import org.apache.storm.tuple.Fields;
import org.apache.storm.utils.Utils;

import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import mprov.alldata.AllDataSpout;
import mprov.alldata.ReadCSVSpout;
import mprov.alldata.SlidingWindowBolt;
import mprov.alldata.producer.ECGdataProducer;

public class StormInstrumentation {
	static File[] csvFiles;
	static Config conf = new Config();
	static LocalCluster cluster;
    
	public static void main(String[] args) {
		//retrieving all files from directory as array
		//String dirName = ECGdataProducer.class.getResource("ECGdataProducer.class").getPath();
		//Path parent = Paths.get(dirName).getParent().getParent().getParent().getParent();
		Path parent = Paths.get("test-data");
		File dir = new File(parent.toString());
		csvFiles =  dir.listFiles(new FilenameFilter() { 
	        public boolean accept(File dir, String filename)
	             { 
	        	return filename.endsWith(".csv"); }
		} );
		
	    // setting configuration
	    conf.setDebug(false);
	    conf.setNumWorkers(1);
	    conf.put(Config.NIMBUS_HOST, "localhost");
	    conf.put(Config.NIMBUS_THRIFT_PORT,6627);
	    conf.put(Config.STORM_ZOOKEEPER_PORT,2181);
	    List<String> zKServers = new ArrayList<String>();
	    zKServers.add("localhost");
	    conf.put(Config.STORM_ZOOKEEPER_SERVERS,zKServers);
//	    System.setProperty("storm.jar", "/Users/rishabhgupta/Documents/mProv/mProv/lib/storm-core-1.1.0.jar");
	    Map<String, Object> defaultStormConf = Utils.readStormConfig();
        defaultStormConf.putAll(conf);

		
	    cluster = new LocalCluster();
	    
	    test();
	    
	    tearDown();
	}

	public static void test() {
		//populating sensor map from all files present in directory
		for (File file:csvFiles){
			String fileName = file.getName();
			String id = fileName.split("\\+")[1];
			String[] nameArr = fileName.split("\\+");
			boolean name=false;
			StringBuffer sensorName = new StringBuffer();
			for(String str:nameArr){
				if(name)
					sensorName.append(str+"+");
				if(str.startsWith("org."))
					name=true;
			}
			MProvFactory.setSensorName(id,sensorName.toString().substring(0, sensorName.length()-4));
		}
		int numData = csvFiles.length;
		
		// building topology
	    TopologyBuilder builder = new TopologyBuilder();
	    builder.setSpout("SPOUT", new ReadCSVSpout(), 1);
	    builder.setBolt("SLIDING_WINDOW_BOLT", 
                new SlidingWindowBolt().withWindow(new Count(15), new Count(5)),
                numData).fieldsGrouping("SPOUT", new Fields("sensor_id"));

	   
		//submit the topology to cluster
		cluster.submitTopology("mProv772", conf, builder.createTopology());
		//sleep
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/*
		//test graph
		ProvToken windowTestToken = new ProvStringToken("w1.11");
		ProvToken resultTestToken = new ProvStringToken("r1.11");
		//System.out.println(prov.getProvenanceLocation(windowTestToken));
		try {
			System.out.println(MProvFactory.getProvenanceStore().getConnectedTo(windowTestToken, "partOf").toString());
			System.out.println(MProvFactory.getProvenanceStore().getProvenanceData(resultTestToken).getValueAt(0));
			System.out.println(MProvFactory.getProvenanceStore().getConnectedFrom(resultTestToken, "wasDerivedFrom").toString());
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(prov.getEdgesFrom(windowTestToken));
		
		*/

	}

	public static void tearDown() {
		//shut down the cluster
		//cluster.shutdown();
	}
}
