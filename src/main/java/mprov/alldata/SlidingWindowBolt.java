package mprov.alldata;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BinaryProvExpression;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier.ProvOperation;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.ProvDmRelation;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.ProvDmType;
import jep.Jep;
import jep.JepConfig;
import jep.JepException;
import mprov.MProvFactory;

public class SlidingWindowBolt extends BaseWindowedBolt {
	Logger logger = LogManager.getLogger(AllDataSpout.class);

	private static final long serialVersionUID = 1L;
	private OutputCollector collector;
	private int prevWindowId = -1;
	private int boltId=0;
	BasicSchema bundleSchema;
	BasicSchema activitySchema;
	BasicSchema resultSchema;
	 
    int windowId = 0;
    int bundleId = 0;
    int resultId = 0;
    Jep jep;

	
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.boltId = context.getThisTaskId();
        
        // creating list of names of fields(keys) for bundle schema
        List<String> fieldNames = new LinkedList<>();
		fieldNames = new LinkedList<>();
	    fieldNames.add("window_info");
	    fieldNames.add("provDmName");
	    
		// creating list of field types for bundle schema
	    List<Class<? extends Object>> types =new LinkedList<>();
	    types = new LinkedList<>();
	    //types.add(Double.class);
	    types.add(String.class);
	    types.add(String.class);
	    
	    // creating bundle schema
	    bundleSchema = new BasicSchema("bundleSchema", fieldNames, types);
        
        
        // creating list of names of fields(keys) for result schema
		fieldNames = new LinkedList<>();
	    fieldNames.add("result_value");
	    fieldNames.add("provDmName");
	    
		// creating list of field types for result schema
	    types = new LinkedList<>();
	    //types.add(Double.class);
	    types.add(String.class);
	    types.add(String.class);
	    
	    // creating result schema
	    resultSchema = new BasicSchema("resultSchema", fieldNames, types);
	    
        // creating list of names of fields(keys) for activity schema
		fieldNames = new LinkedList<>();
	    fieldNames.add("activity_name");
	    fieldNames.add("provDmName");
	    
		// creating list of field types for activity schema
	    types = new LinkedList<>();
	    //types.add(Double.class);
	    types.add(String.class);
	    types.add(String.class);
	    
	    // creating result schema
	    activitySchema = new BasicSchema("activitySchema", fieldNames, types);
	    
	    
	    try {
			jep = new Jep(new JepConfig().addSharedModules("numpy").addSharedModules("scipy").addSharedModules("findspark").addSharedModules("py4j"));
            jep.eval("from java.lang import System");
            jep.eval("import sys");
            jep.eval("import os");
            jep.eval("os.environ['SPARK_HOME']='/usr/local/Cellar/apache-spark/2.2.0'");
            jep.eval("os.environ['PYTHONPATH']=os.environ['SPARK_HOME']+'/libexec/python/lib:'+os.environ['SPARK_HOME']+'/libexec/python'+os.environ['PATH']");
//            jep.eval("import findspark");
//            jep.eval("findspark.init()");
            jep.eval("os.environ['PYSPARK_PYTHONPATH_SET']='1'");
            jep.eval("sys.path.append(\"/Users/zives/habitat/CerebralCortex-APIServer/cerebralcortex\")");
            jep.eval("sys.path.append(\"/usr/local/Cellar/apache-spark/2.2.0/libexec/python\")");
            jep.eval("sys.path.append(\"/usr/local/Cellar/apache-spark/2.2.0/libexec/python/lib\")");
		} catch (JepException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Override
    public void execute(TupleWindow inputWindow) {
    	
    	// incrementing the IDs
    	String window = MProvFactory.getWindowID(String.valueOf(boltId), windowId);
    	String result = MProvFactory.getResultID(String.valueOf(boltId), resultId);
    	String activity = MProvFactory.getActivityID(String.valueOf(boltId), resultId);
    	String prevWindow = MProvFactory.getWindowID(String.valueOf(boltId), windowId);
    	
    	//iterating through values in the input tuple window
    	List<Tuple> list = inputWindow.get();
    	List<ProvToken> tokenList = new ArrayList<>();
    	
    	for(Tuple entity: list){
    		//creating token of each tuple value and adding it to the list
    		ProvToken token = new ProvStringToken((String)entity.getValueByField("id")) ;
    		tokenList.add(token);
    	}
    	//creating result token
		ProvToken resultToken = new ProvStringToken(result);
		
    	   //creating window token and storing the link between entity token and window token
     	ProvToken windowToken = new ProvStringToken(window);
	    
		List<Integer> locationIntList = new ArrayList<>();
	    locationIntList.add(Integer.parseInt((String)list.get(0).getValueByField("sensor_id")));
	    locationIntList.add(Integer.parseInt(((String)list.get(0).getValueByField("id")).substring(2)));
	    locationIntList.add(Integer.parseInt(((String)list.get(list.size()-1).getValueByField("id")).substring(2)));
	    ProvSpecifier location = new ProvLocation("windowIdx", locationIntList);
		
        List<Field<? extends Object>> valueFields = new LinkedList<>();
        StringField window_info = new StringField(locationIntList.toString());
        StringField provDmName = new StringField(ProvDmType.COLLECTION.name());
    	    valueFields.add(window_info);
    	    valueFields.add(provDmName);
    	    TupleWithSchema<String> tuple = bundleSchema.createTuple();
    	    tuple.setFields(valueFields);
    	    
		//storing window token
	    //NEO4J
		try {
			MProvFactory.getProvenanceStore().storeProvenanceNode(windowToken, tuple, location);
    		logger.info("node, window, "+window + ", "+location);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        valueFields = new LinkedList<>();
        StringField activity_info = new StringField("cStress");
        provDmName = new StringField(ProvDmType.ACTIVITY.name());
    	    valueFields.add(activity_info);
    	    valueFields.add(provDmName);
    	    tuple = activitySchema.createTuple();
    	    tuple.setFields(valueFields);
		
		ProvStringToken activityToken = new ProvStringToken(activity);
    	    
		//storing activity token
	    //NEO4J
		try {
			MProvFactory.getProvenanceStore().storeProvenanceNode(activityToken, tuple, location);
    		logger.info("node, activity, "+activity_info + ", "+location);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	//storing link between window token and entity tokens
    	//NEO4J
	
	    try {


		for (ProvToken inList: tokenList){
		    	MProvFactory.getProvenanceStore().storeProvenanceLink(windowToken, inList, ProvDmRelation.MEMBERSHIP.name());
		    	logger.info("edge, partOf, "+inList.toString() + ", "+window);
		    	}
		
	 	MProvFactory.getProvenanceStore().storeProvenanceLink(activityToken, windowToken, ProvDmRelation.USAGE.name());
	 	MProvFactory.getProvenanceStore().storeProvenanceLink(resultToken, activityToken, ProvDmRelation.GENERATION.name());

		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
    	// storing link between current window token and previous window token
    	ProvToken prevWindowToken = new ProvStringToken(MProvFactory.getWindowID(String.valueOf(boltId), prevWindowId));
    		//NEO4J
    		
	 	try {
			MProvFactory.getProvenanceStore().storeProvenanceLink(windowToken, prevWindowToken, "precededBy");
			logger.info("edge, precededBy, " + window + ", "+ prevWindow);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
    	System.out.println("getting Stress Level");
    	
    	getStressLevel(inputWindow, windowToken, resultToken, location);
    	//calculating and storing result in the graph
    	//calculateResult(inputWindow, windowToken, resultToken, location);
    	
    	// emitting the values of window
    	collector.emit(new Values("window",windowId,(String)inputWindow.get().get(0).getValueByField("sensor_name"),inputWindow));
    	
    	//storing current window id as previous window id for use during next window computation
    	prevWindowId = windowId;
    	windowId++;
    	resultId++;
    }
    private void getStressLevel(TupleWindow inputWindow, ProvToken windowToken, ProvToken resultToken, ProvSpecifier location) {
        try {
        		List<Tuple> list = inputWindow.get();
        		List<String> ecg_list = new ArrayList<>();
        		List<String> accel_x_list = new ArrayList<>();
        		int count=0;
	        	for(Tuple entity: list){
	        		String data = (String) entity.getValueByField("data");
	        		String[] ecg_vals = data.split("^^^^")[0].split(",");
	        		String[] accel_x_vals = data.split("^^^^")[0].split(",");
	        		ecg_list.add(ecg_vals[ecg_vals.length-1] + " " + String.valueOf(2000+count));
	        		accel_x_list.add(accel_x_vals[accel_x_vals.length-1] + " " + String.valueOf(2000+count));
	        		count++;
	        	}

//            List<Tuple> ecg = ecg_window.get();
//            List<Tuple> accel_x = accel_x_window.get();
//            List<Tuple> accel_y = accel_y_window.get();
//            List<Tuple> accel_z = accel_z_window.get();
//            List<Tuple> rip = rip_window.get();
	        	
	        	StringBuffer ecg_list_strB = new StringBuffer();
	        	for(int i=0;i<ecg_list.size();i++) {
		        	ecg_list_strB.append("parser.data_processor('"+ecg_list.get(i)+"'), ");
	        	}
	        String 	ecg_list_str = ecg_list_strB.toString().substring(0,ecg_list_strB.length()-2);
	        
		    	StringBuffer accel_x_list_strB = new StringBuffer();
	        	for(int i=0;i<ecg_list.size();i++) {
	        		accel_x_list_strB.append("parser.data_processor('"+accel_x_list.get(i)+"'), ");
	        	}
	        String 	accel_x_list_str = accel_x_list_strB.toString().substring(0,accel_x_list_strB.length()-2);
            jep.eval("import numpy");
            jep.eval("import scipy");
            
            jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct");
            jep.eval("from cerebralcortex.kernel.datatypes.datastream import DataStream");
            jep.eval("from cerebralcortex.data_processor.preprocessor import parser");
            jep.eval("from datetime import datetime, timedelta");
            jep.eval("start_time_val=datetime.now()");
            jep.eval("end_time_val=datetime.now() + timedelta(hours=2)");
            jep.eval("from cerebralcortex.data_processor.feature.ecg import ecg_feature_computation");
            jep.eval("from cerebralcortex.data_processor.feature.feature_vector import generate_cStress_feature_vector");
            jep.eval("from cerebralcortex.data_processor.feature.rip import rip_cycle_feature_computation");
            jep.eval("from cerebralcortex.data_processor.feature.rip import window_rip");
            jep.eval("from cerebralcortex.data_processor.signalprocessing import rip");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.accelerometer import accelerometer_features");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct, autosense_sequence_align");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import compute_outlier_ecg");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import ecg_data_quality");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import rip_data_quality");
            jep.eval("from cerebralcortex.data_processor.signalprocessing.ecg import compute_rr_intervals");
            jep.eval("from cerebralcortex.model_development.model_development import analyze_events_with_features");

            
            System.out.println(ecg_list.toString());
            System.out.println("ecg_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+ecg_list_str+"], start_time = start_time_val, end_time = end_time_val)");
            jep.eval("ecg_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+ecg_list_str+"], start_time = start_time_val, end_time = end_time_val)");
            jep.eval("accel_x_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+accel_x_list_str+"], start_time = start_time_val, end_time = end_time_val)");
            jep.eval("accel_y_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+accel_x_list_str+"], start_time = start_time_val, end_time = end_time_val)");
            jep.eval("accel_z_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+accel_x_list_str+"], start_time = start_time_val, end_time = end_time_val)");
            jep.eval("rip_data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = ["+accel_x_list_str+"], start_time = start_time_val, end_time = end_time_val)");
    
            jep.eval("from cerebralcortex.data_processor.cStress import cStress");
            jep.eval("ecg_sampling_frequency = 64.0");
            jep.eval("rip_sampling_frequency = 64.0");
            jep.eval("accel_sampling_frequency = 64.0/6.0");
            System.out.println("ecg_data"+jep.getValue("ecg_data"));
            System.out.println("accel_x_data"+jep.getValue("accel_x_data"));
            jep.eval("ecg_corrected = timestamp_correct(ecg_data, ecg_sampling_frequency)");
            jep.eval("rip_corrected = timestamp_correct(rip_data, rip_sampling_frequency)");
            
            
            jep.eval("accelx_corrected = timestamp_correct(datastream=accel_x_data, sampling_frequency=accel_sampling_frequency)");
			jep.eval("accely_corrected = timestamp_correct(datastream=accel_y_data, sampling_frequency=accel_sampling_frequency)");
			jep.eval("accelz_corrected = timestamp_correct(datastream=accel_z_data, sampling_frequency=accel_sampling_frequency)");
				
			
            jep.eval("ecg_quality = ecg_data_quality(ecg_corrected)");
            jep.eval("rip_quality_val = rip_data_quality(rip_corrected)");
            System.out.println(jep.getValue("ecg_corrected"));
            System.out.println(jep.getValue("rip_corrected"));
            System.out.println(jep.getValue("ecg_quality"));
            System.out.println(jep.getValue("rip_quality_val"));
            jep.eval("accel=autosense_sequence_align(datastreams=[accelx_corrected, accely_corrected, accelz_corrected], sampling_frequency=accel_sampling_frequency)");
            jep.eval("accel_features=accelerometer_features(accel, window_length=1)");

            jep.eval("ecg_rr_rdd = compute_rr_intervals(ecg=ecg_corrected,ecg_quality=ecg_quality,fs=ecg_sampling_frequency)");
            List<Field<? extends Object>> valueFields = new LinkedList<>();
            StringField tuple_result_val = new StringField((String)jep.getValue("ecg_quality"));
            StringField provDmName = new StringField(ProvDmType.ENTITY.name());
	    	    valueFields.add(tuple_result_val);
	    	    valueFields.add(provDmName);
	    	    TupleWithSchema<String> tuple = resultSchema.createTuple();
	    	    tuple.setFields(valueFields);
            try {
        		
    			//storing the result token as node in graph
				MProvFactory.getProvenanceStore().storeProvenanceNode(resultToken, tuple, location);
				logger.info("node, result, "+resultToken.toString() + ", "+tuple.toString());

	    		//storing the link between result and window tokens in graph
	    		MProvFactory.getProvenanceStore().storeProvenanceLink(resultToken, windowToken, ProvDmRelation.DERIVATION.name());
	    		logger.info("node, result, "+resultToken.toString() + ", "+tuple.toString());
	    		
    		} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
/*            
            jep.eval("ecg_rr_quality = compute_outlier_ecg(ecg_rr_rdd)");
            //System.out.println(jep.getValue("ecg_rr_rdd"));
            //System.out.println(jep.getValue("ecg_rr_quality"));
            jep.eval("windowed_ecg_features = ecg_feature_computation(datastream=ecg_corrected, quality_datastream=ecg_quality, window_size=1, window_offset=1)");      
            //System.out.println(jep.getValue("windowed_ecg_features"));
            jep.eval("feature_vector_ecg_rip = generate_cStress_feature_vector(windowed_ecg_features)");
            System.out.println("feature_vector_ecg_rip");
            System.out.println(jep.getValue("feature_vector_ecg_rip"));
            */
        } catch (JepException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    /*private void calculateResult(TupleWindow inputWindow, ProvToken windowToken, ProvToken resultToken, ProvSpecifier location){
    	List<Tuple> list = inputWindow.get();
    	double sum = 0;
    	int count = 0;
		
    	// selecting computation based on the sensor type
    	switch((String)inputWindow.get().get(0).getValueByField("sensor_name")){
    	case "GALVANIC_SKIN_RESPONSE+AUTOSENSE_CHEST+CHEST.":
    		//calculating sum and count of records for mean
        	for(Tuple entity: list){
        		String data = (String) entity.getValueByField("data");
        		String[] dataValsStr= data.split(",");
        		sum+=Double.valueOf(dataValsStr[dataValsStr.length-1]);
        		count++;
        	}
        	double mean = sum/count;
        	
    		//creating tuple with result schema and setting list of values
    	    List<Field<? extends Object>> valueFields = new LinkedList<>();
    	    DoubleField tuple_result_val = new DoubleField(mean);
    	    valueFields.add(tuple_result_val);
    	    TupleWithSchema<String> tuple = resultSchema.createTuple();
    	    tuple.setFields(valueFields);
    	    
    		//NEO4J
    		 try {
	    	    //storing the result token as node in graph
    	    	
	    	    MProvFactory.getProvenanceStore().storeProvenanceNode(resultToken, tuple, location);
	    	    logger.info("node, result, "+resultToken.toString() + ", "+tuple.toString());
	    		
	    		//storing the link between result and window token in graph
	    	    MProvFactory.getProvenanceStore().storeProvenanceLink(windowToken, resultToken, "derives");
	    	    logger.info("edge, derives, "+windowToken.toString() + ", "+resultToken.toString());
	    		
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        	break;
    	case "GYROSCOPE+PHONE.":
    		//calculating sum and count of records for mean
        	for(Tuple entity: list){
        		String data = (String) entity.getValueByField("data");
        		String[] dataValsStr= data.split(",");
        		sum+=Double.valueOf(dataValsStr[dataValsStr.length-1]);
        		count++;
        	}
        	mean = sum/count;
    		
    		//creating tuple with result schema and setting list of values
    	    valueFields = new LinkedList<>();
    	    tuple_result_val = new DoubleField(mean);
    	    valueFields.add(tuple_result_val);
    	    tuple = resultSchema.createTuple();
    	    tuple.setFields(valueFields);
    	    
    		//NEO4J
    		try {
    		
    			//storing the result token as node in graph
				MProvFactory.getProvenanceStore().storeProvenanceNode(resultToken, tuple, location);
				logger.info("node, result, "+resultToken.toString() + ", "+tuple.toString());

	    		//storing the link between result and window tokens in graph
	    		MProvFactory.getProvenanceStore().storeProvenanceLink(windowToken, resultToken, "derives");
	    		logger.info("node, result, "+resultToken.toString() + ", "+tuple.toString());
	    		
    		} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	break;
    	}
    }*/
    
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("name","id","sensorName","tuplewindow"));
    }
    
    @Override
    public void cleanup() {
        jep.close();
    }
}
