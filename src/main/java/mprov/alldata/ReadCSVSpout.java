package mprov.alldata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.db.habitat.repository.prov.parser.data.ProvDmType;
import mprov.MProvFactory;

public class ReadCSVSpout extends BaseRichSpout {
	Logger logger = LogManager.getLogger(ReadCSVSpout.class);

	KafkaConsumer<String, String> consumer;

	private static final long serialVersionUID = 1L;

	SpoutOutputCollector collector;
	BasicSchema entitySchema;
	boolean processedOnce = false;
	int i = 0;
	private int boltId = 1;
	int count=0;
	File ecg_csvFile;
	File accel_x_csvFile;
	File accel_y_csvFile;
	File accel_z_csvFile;
	File rip_csvFile;						

			BufferedReader br_ecg = null;
			BufferedReader br_accel_x = null;
	
	@Override
	public void nextTuple() {
				String sensor_id = ecg_csvFile.getName().split("\\+")[1];
		        String strLine_ecg, strLine_accel_x, strLine = "";
		
   
		        //reading records from file
		        try {
					if( (strLine_ecg = br_ecg.readLine()) != null && ((strLine_accel_x = br_ecg.readLine()) != null) && count<10000)
					{
					   strLine = strLine_ecg + "^^^^"+strLine_accel_x;
				       //System.out.println(sensor_id+"---" + strLine + "\nMessage sent successfully"); 
				    
					    // iterating through records read
					   count++;
			
						// writing graph data temporarily to file
					//	logger.info(
								//"node, entity," + sensor_id + ", " + MProvFactory.getSensorName(sensor_id) + ", " + strLine);
			
						// creating token
						String tokenId = MProvFactory.getEntityID(String.valueOf(boltId), i);
						ProvStringToken token = new ProvStringToken(tokenId);
			
						// creating a list of values (for tuple) having the attributes and
						// data of that entity
						List<Field<? extends Object>> valueFields = new LinkedList<>();
						IntField tuple_id_val = new IntField(i);
						StringField sensor_name = new StringField(MProvFactory.getSensorName(sensor_id));
						StringField sensor_value = new StringField(strLine);
						StringField provDmName = new StringField(ProvDmType.ENTITY.name());
						valueFields.add(tuple_id_val);
						valueFields.add(sensor_name);
						valueFields.add(sensor_value);
						valueFields.add(provDmName);
			
						// creating location for that token
						List<Integer> locationIntList = new ArrayList<>();
						locationIntList.add(Integer.valueOf(sensor_id));
						locationIntList.add(i);
						locationIntList.add(-1);
						ProvSpecifier location = new ProvLocation("entityIdx", locationIntList);
			
						// creating tuple from schema and setting the values
						BasicTuple tuple = entitySchema.createTuple();
						tuple.setFields(valueFields);
						
						//System.out.println("here1");
						// storing token (with tuple of values and location) in the graph as
						// a node
					    //NEO4J
						try {
							MProvFactory.getProvenanceStore().storeProvenanceNode(token, tuple, location);
						} catch (HabitatServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						//System.out.println("here2");
						// emitting tuple
						String sensor_name_str = MProvFactory.getSensorName(sensor_id);
						collector.emit(new Values("entity", tokenId, sensor_id, sensor_name_str, strLine));
						processedOnce = true;
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
				}



	@Override
	public void open(Map map, TopologyContext context, SpoutOutputCollector collector) {
		//this.boltId = context.getThisTaskId();
		this.collector = collector;

		// creating list of names of fields(keys) for entity schema
		List<String> fields = new LinkedList<>();
		fields.add("tuple_id");
		fields.add("sensor_id");
		fields.add("sensor_value");
		fields.add("provDmName");

		// creating list of field types for entity schema
		List<Class<? extends Object>> types = new LinkedList<>();
		types.add(Integer.class);
		types.add(String.class);
		types.add(String.class);
		types.add(String.class);

		// creating a schema for entity
		entitySchema = new BasicSchema("entitySchema", fields, types);

		ecg_csvFile = new File("test-data/0b730a24-1a01-3bde-b3ec-bbc77dcf3891+7225+org.md2k.autosense+ECG+AUTOSENSE_CHEST+CHEST.csv");
		accel_x_csvFile = new File("test-data/0b730a24-1a01-3bde-b3ec-bbc77dcf3891+7230+org.md2k.autosense+ACCELEROMETER_X+AUTOSENSE_CHEST+CHEST.csv");
		accel_y_csvFile = new File("test-data/0b730a24-1a01-3bde-b3ec-bbc77dcf3891+7233+org.md2k.autosense+ACCELEROMETER_Y+AUTOSENSE_CHEST+CHEST.csv");
		accel_z_csvFile = new File("test-data/0b730a24-1a01-3bde-b3ec-bbc77dcf3891+7236+org.md2k.autosense+ACCELEROMETER_Z+AUTOSENSE_CHEST+CHEST.csv");
		rip_csvFile = new File("test-data/0b730a24-1a01-3bde-b3ec-bbc77dcf3891+7272+org.md2k.streamprocessor+ORG_MD2K_CSTRESS_DATA_RIP_QUALITY+PHONE.csv");						

		br_ecg = null;
		br_accel_x = null;
				try {
					br_ecg = new BufferedReader( new FileReader(ecg_csvFile));
					br_accel_x = new BufferedReader( new FileReader(rip_csvFile));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("name", "id", "sensor_id", "sensor_name", "data"));
	}

}
