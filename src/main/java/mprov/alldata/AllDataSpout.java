package mprov.alldata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import mprov.MProvFactory;

public class AllDataSpout extends BaseRichSpout {
	Logger logger = LogManager.getLogger(AllDataSpout.class);

	KafkaConsumer<String, String> consumer;

	private static final long serialVersionUID = 1L;

	SpoutOutputCollector collector;
	BasicSchema entitySchema;
	boolean processedOnce = false;
	int i = 0;
	private int boltId = 1;

	@Override
	public void nextTuple() {

		System.out.println("Waiting for data");
		// reading records from Kafka Consumer
		ConsumerRecords<String, String> records = consumer.poll(100);
		
		// iterating through records read
		for (ConsumerRecord<String, String> record : records) {
			i++;
			String sensor_id = record.value().split("---")[0]; // getting sensor
																// id
			String recordValue = record.value().split("---")[1]; // getting the
																	// recorded
																	// data

			// writing graph data temporarily to file
			logger.info(
					"node, entity," + sensor_id + ", " + MProvFactory.getSensorName(sensor_id) + ", " + recordValue);

			// creating token
			String tokenId = MProvFactory.getEntityID(String.valueOf(boltId), i);
			ProvStringToken token = new ProvStringToken(tokenId);

			// creating a list of values (for tuple) having the attributes and
			// data of that entity
			List<Field<? extends Object>> valueFields = new LinkedList<>();
			IntField tuple_id_val = new IntField(i);
			StringField sensor_name = new StringField(MProvFactory.getSensorName(sensor_id));
			StringField sensor_value = new StringField(recordValue);
			valueFields.add(tuple_id_val);
			valueFields.add(sensor_name);
			valueFields.add(sensor_value);

			// creating location for that token
			List<Integer> locationIntList = new ArrayList<>();
			locationIntList.add(Integer.valueOf(sensor_id));
			locationIntList.add(i);
			locationIntList.add(-1);
			ProvSpecifier location = new ProvLocation("entityIdx", locationIntList);

			// creating tuple from schema and setting the values
			BasicTuple tuple = entitySchema.createTuple();
			tuple.setFields(valueFields);
			
			System.out.println("here1");
			// storing token (with tuple of values and location) in the graph as
			// a node
			try {
				MProvFactory.getProvenanceStore().storeProvenanceNode(token, tuple, location);
			} catch (HabitatServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("here2");
			// emitting tuple
			String sensor_name_str = MProvFactory.getSensorName(sensor_id);
			collector.emit(new Values("entity", tokenId, sensor_id, sensor_name_str, recordValue));
			processedOnce = true;
		}

	}

	@Override
	public void open(Map map, TopologyContext context, SpoutOutputCollector collector) {
		//this.boltId = context.getThisTaskId();
		this.collector = collector;

		// Kafka consumer configuration settings
		String topicName = "mprov";
		Properties props = new Properties();

		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		consumer = new KafkaConsumer<String, String>(props);
		
		System.out.println("created consumer");
		// Kafka Consumer subscribes list of topics here.
		consumer.subscribe(Arrays.asList(topicName));

		// print the topic name
		System.out.println("Subscribed to topic " + topicName);

		// creating list of names of fields(keys) for entity schema
		List<String> fields = new LinkedList<>();
		fields.add("tuple_id");
		fields.add("sensor_id");
		fields.add("sensor_value");

		// creating list of field types for entity schema
		List<Class<? extends Object>> types = new LinkedList<>();
		types.add(Integer.class);
		types.add(String.class);
		types.add(String.class);

		// creating a schema for entity
		entitySchema = new BasicSchema("entitySchema", fields, types);

	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("name", "id", "sensor_id", "sensor_name", "data"));
	}

}
