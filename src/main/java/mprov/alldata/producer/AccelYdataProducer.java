package mprov.alldata.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class AccelYdataProducer {

	public static void main(String[] args) {

		//Kafka producer
		
		// TODO: fix this to match the path selection in TestStormInstrumentation
		
//		String dirName = "/Users/rishabhgupta/Downloads/mProv_test_data";
//		File dir = new File(dirName);
		
		//getting all files in a directory
//		File[] csvFiles =  dir.listFiles(new FilenameFilter() { 
//	        public boolean accept(File dir, String filename)
//	             { return filename.endsWith(".csv"); }
//		} );
		String dirName = AccelYdataProducer.class.getResource("AccelYDataProducer.class").getPath();
		Path parent = Paths.get(dirName).getParent().getParent().getParent().getParent().getParent().getParent();
        System.out.println(parent);
		File dir = new File(parent.toString()+"/test-data");
		File[] csvFiles =  dir.listFiles(new FilenameFilter() { 
	        public boolean accept(File dir, String filename)
	             { 
	        	return filename.endsWith(".csv"); }
		} );
		
        String topicName = "accel_y";
        
		  
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", 
           "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", 
           "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = new KafkaProducer
                <String, String>(props);
		//reading records of each file in directory
		for(File csvFile:csvFiles)
		{ 
			if(csvFile.getName().contains("ACCELEROMETER_Y")) {
				BufferedReader br = null;
				try {
					br = new BufferedReader( new FileReader(dir + "/"+ csvFile.getName()));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String sensor_Id = csvFile.getName().split("\\+")[1];
		        String strLine = "";
		
		        int i=0;   
		        //reading records from file
		        try {
					while( (strLine = br.readLine()) != null)
					{
		
				             producer.send(new ProducerRecord<String, String>(topicName, 
				                Integer.toString(i), sensor_Id+"---"+strLine));
				                   System.out.println(sensor_Id+"---"+strLine+"\nMessage sent successfully"); 
					    
					    
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
	        producer.close();
		}
	
	}
	
	

}
