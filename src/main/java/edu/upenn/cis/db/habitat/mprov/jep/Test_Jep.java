package edu.upenn.cis.db.habitat.mprov.jep;


import jep.Jep;
import jep.JepConfig;
import jep.JepException;

public class Test_Jep {

	
	public static void main(String[] args){
		try(Jep jep = new Jep(new JepConfig().addSharedModules("numpy").addSharedModules("scipy"))) {
			jep.eval("from java.lang import System");
		    jep.eval("import numpy");
		    jep.eval("import scipy");
			jep.eval("import sys");
			jep.eval("import os");
			jep.eval("os.environ['SPARK_HOME']='/usr/local/spark'");
			jep.eval("os.environ['PYTHONPATH']=os.environ['SPARK_HOME']+'/python/lib/py4j-0.10.4-src.zip:'+os.environ['SPARK_HOME']+'/python:'+os.environ['PATH']");
			jep.eval("import findspark");
			jep.eval("findspark.init()");
			jep.eval("os.environ['PYSPARK_PYTHONPATH_SET']='1'");
			jep.eval("sys.path.append(\"/Users/rishabhgupta/GitHub/CerebralCortex\")");
			jep.eval("sys.path.append(\"/usr/local/spark/python\")");
			jep.eval("sys.path.append(\"/usr/local/spark/python/lib\")");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct");
			jep.eval("from cerebralcortex.kernel.datatypes.datastream import DataStream");
			jep.eval("from datetime import datetime, timedelta");
			jep.eval("start_time_val=datetime.now()");
			jep.eval("end_time_val=datetime.now() + timedelta(hours=9)");
			jep.eval("from cerebralcortex.data_processor.preprocessor import parser");
			jep.eval("data=DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = [parser.data_processor('1.1')], start_time = start_time_val, end_time = end_time_val)");
			jep.eval("dataListItem1={'participant': 'ab123', 'ecg': data, 'rip': data, 'accelx': data, 'accely': data, 'accelz': data, 'stress_marks': data}");
			jep.eval("dataListItem2={'participant': 'ab123', 'ecg': data, 'rip': data, 'accelx': data, 'accely': data, 'accelz': data, 'stress_marks': data}");
			jep.eval("finalData=[dataListItem1, dataListItem2]");
			//jep.eval("data10 = DataStream(identifier='ab123', owner = 'ab123', name = 'ab123', data_descriptor= ['x', 'y', 'z'], data = [parser.data_processor('1.1'), parser.data_processor('0.5'), parser.data_processor('2.5'), parser.data_processor('1.1'), parser.data_processor('0.2'), parser.data_processor('1.1'), parser.data_processor('1.1'), parser.data_processor('3.2'), parser.data_processor('1.1'), parser.data_processor('1.1'), parser.data_processor('1.1'), parser.data_processor('1.1')], start_time = start_time_val, end_time = end_time_val)");
			jep.eval("data10=data");
			System.out.println(jep.getValue("data10"));
			jep.eval("from cerebralcortex.data_processor.cStress import cStress");
			jep.eval("ecg_sampling_frequency = 2");
			jep.eval("rip_sampling_frequency = 3");
			jep.eval("accel_sampling_frequency = 0.5");
			jep.eval("ecg_corrected = timestamp_correct(data10, ecg_sampling_frequency)");
			jep.eval("rip_corrected = timestamp_correct(data10, rip_sampling_frequency)");
			
			jep.eval("accelx_corrected = timestamp_correct(datastream=data10, sampling_frequency=accel_sampling_frequency)");
			jep.eval("accely_corrected = timestamp_correct(datastream=data10, sampling_frequency=accel_sampling_frequency)");
			jep.eval("accelz_corrected = timestamp_correct(datastream=data10, sampling_frequency=accel_sampling_frequency)");
					

			jep.eval("from cerebralcortex.data_processor.feature.ecg import ecg_feature_computation");
			jep.eval("from cerebralcortex.data_processor.feature.feature_vector import generate_cStress_feature_vector");
			jep.eval("from cerebralcortex.data_processor.feature.rip import rip_cycle_feature_computation");
			jep.eval("from cerebralcortex.data_processor.feature.rip import window_rip");
			jep.eval("from cerebralcortex.data_processor.signalprocessing import rip");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.accelerometer import accelerometer_features");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.alignment import timestamp_correct, autosense_sequence_align");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import compute_outlier_ecg");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import ecg_data_quality");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.dataquality import rip_data_quality");
			jep.eval("from cerebralcortex.data_processor.signalprocessing.ecg import compute_rr_intervals");
			jep.eval("from cerebralcortex.model_development.model_development import analyze_events_with_features");
			
			jep.eval("ecg_quality = ecg_data_quality(ecg_corrected)");
			System.out.println(jep.getValue("ecg_quality"));
			jep.eval("rip_quality_val = rip_data_quality(rip_corrected)");
			jep.eval("accel=autosense_sequence_align(datastreams=[accelx_corrected, accely_corrected, accelz_corrected], sampling_frequency=accel_sampling_frequency)");
			jep.eval("accel_features=accelerometer_features(accel, window_length=1)");
			//can't find window_accel
			//jep.eval("windowed_accel_features = window_accel(accel_features,window_size=5)");
			//jep.eval("peak_valley = rip.compute_peak_valley(rip=rip_corrected, rip_quality=rip_quality_val)");
			//jep.eval("rip_cycle_features = rip_cycle_feature_computation(peak_valley[0])");
			//jep.eval("windowed_rip_features = window_rip(peak_datastream=rip_cycle_features[0],valley_datastream=rip_cycle_features[1],inspiration_duration=rip_cycle_features[2],inspiration_duration=rip_cycle_features[3],respiration_duration=rip_cycle_features[4],inspiration_expiration_ratio=rip_cycle_features[5],stretch=rip_cycle_features[6], window_size=5, window_offset=5)");
			jep.eval("ecg_rr_rdd = compute_rr_intervals(ecg=ecg_corrected,ecg_quality=ecg_quality,fs=ecg_sampling_frequency)");
			jep.eval("ecg_rr_quality = compute_outlier_ecg(ecg_rr_rdd)");
			System.out.println(jep.getValue("ecg_rr_quality"));
			jep.eval("windowed_ecg_features = ecg_feature_computation(datastream=ecg_rr_rdd, quality_datastream=ecg_rr_quality, window_size=1, window_offset=1)");		
			System.out.println(jep.getValue("windowed_ecg_features"));
			jep.eval("feature_vector_ecg_rip = generate_cStress_feature_vector(windowed_ecg_features)");
			System.out.println(jep.getValue("feature_vector_ecg_rip"));
		} catch (JepException je) {
			je.printStackTrace();;
		}
		finally {
			//sc.close();
		}		
	}
}
